import { FC, ReactElement } from "react";
import { useTranslation } from "next-i18next";

interface TranslateProps {
  k: string;
  namespace?: string;
}

const Translate: FC<TranslateProps> = ({
  k,
  namespace = "common",
}): ReactElement => {
  const { t } = useTranslation(namespace);
  return <>{t(k)}</>;
};

export default Translate;
