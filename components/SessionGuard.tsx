import React, { FC, ReactElement, useEffect, useRef, useState } from "react";
import Box from "@mui/material/Box";
import { apiBaseUrl } from "../lib/consts";
import { Button, MenuItem, TextField, Typography } from "@mui/material";
import Translate from "./Translate";

interface SessionGuardProps {
  children: ReactElement;
}

const SessionGuard: FC<SessionGuardProps> = ({ children }): ReactElement => {
  const [validationState, setValidationState] = useState("loading");

  const recipients = process.env.NEXT_PUBLIC_EMAIL_RECIPIENTS ?? "";

  const inputRef = useRef<HTMLInputElement | null>(null);
  const emailRef = useRef<HTMLSelectElement | null>(null);

  const validateSession = async (sessionCode: string) => {
    try {
      const response = await fetch(`${apiBaseUrl}/session`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        method: "POST",
        body: JSON.stringify({ sessionCode }),
      });
      const data = await response.json();
      if (data.ok) {
        setValidationState("valid");
        localStorage.setItem("sessionCode", sessionCode);
      } else {
        setValidationState("invalid");
      }
    } catch (err) {}
  };

  const startNewSession = async () => {
    const selectedEmail = emailRef?.current?.value;
    if (selectedEmail) {
      try {
        await fetch(`${apiBaseUrl}/session?email=${selectedEmail}`, {
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
        });
      } catch (err) {}
      localStorage.setItem("sessionEmail", selectedEmail);
    }
  };

  const handleSubmit = () => {
    const sessionCode = inputRef.current?.value;
    if (sessionCode) {
      setValidationState("loading-submit");
      validateSession(sessionCode);
    }
  };

  useEffect(() => {
    const sessionCode = localStorage.getItem("sessionCode");
    if (sessionCode) {
      validateSession(sessionCode);
    } else {
      setValidationState("");
    }
  }, []);

  const inputError = !!inputRef.current?.value && validationState === "invalid";
  if (validationState === "loading") {
    return (
      <Box mt="300px">
        <Typography variant="h3" textAlign="center">
          <Translate k="validating" namespace="backoffice" />
        </Typography>
      </Box>
    );
  }

  if (validationState === "valid") {
    return <Box>{children}</Box>;
  }

  return (
    <Box mt="300px">
      <TextField
        error={inputError}
        helperText={
          inputError && <Translate k="codeNotValid" namespace="backoffice" />
        }
        inputRef={inputRef}
        label={<Translate k="sessionCode" namespace="backoffice" />}
        sx={{
          "& p": {
            fontWeight: 500,
            textAlign: "right",
            fontSize: "12px",
            lineHeight: "16px",
            letterSpacing: "0.2px",
          },
        }}
      />
      <br /> <br />
      <Button onClick={handleSubmit}>
        <Translate
          k={validationState === "loading-submit" ? "validating" : "validate"}
          namespace="backoffice"
        />
      </Button>
      <br /> <br /> <br />
      <br />
      <TextField
        select
        inputRef={emailRef}
        defaultValue={localStorage.getItem("sessionEmail") ?? ""}
        label={<Translate k="email" namespace="backoffice" />}
        sx={{
          mb: "22px",
          width: "380px",
          "& div": {
            textAlign: "left",
          },
        }}
      >
        {recipients.split(",").map((value) => {
          return (
            <MenuItem key={value} value={value.trim()}>
              {value.trim()}
            </MenuItem>
          );
        })}
      </TextField>
      <br />
      <Button variant="text" onClick={startNewSession}>
        <Translate k="sendNewCode" namespace="backoffice" />
      </Button>
    </Box>
  );
};

export default SessionGuard;
