import React, { FC, ReactElement, useState } from "react";
import Box from "@mui/material/Box";
import * as Yup from "yup";
import FormFields from "./FormFields";
import { Form, Formik } from "formik";
import { apiBaseUrl } from "../../lib/consts";
import Button from "@mui/material/Button";
import { BlogType } from "../../lib/api/blog";
import Translate from "../Translate";
import { Dialog, Typography } from "@mui/material";
import { useRouter } from "next/router";

interface BlogFormProps {
  blog?: BlogType;
}

export const initialValues: Record<string, string | Object> = {
  src: "",
  srcMin: "",
  sr: {
    title: "",
    subtitle: "",
    content: "",
  },
  en: {
    title: "",
    subtitle: "",
    content: "",
  },
};

export type InitialValues = typeof initialValues;

const validationSchema = Yup.object().shape({
  src: Yup.string().required("required"),
  srcMin: Yup.string().required("required"),
  sr: Yup.object().shape({
    title: Yup.string().required("required"),
    subtitle: Yup.string().required("required"),
    content: Yup.string().required("required"),
  }),
  en: Yup.object().shape({
    title: Yup.string().required("required"),
    subtitle: Yup.string().required("required"),
    content: Yup.string().required("required"),
  }),
});

const BlogForm: FC<BlogFormProps> = ({ blog }): ReactElement => {
  const { query, replace, locale } = useRouter();
  const [deleteBlogState, setDeleteBlogState] = useState<
    "loading" | "open" | null
  >(null);
  const method = blog ? "PATCH" : "POST";

  const handleSubmit = async (values: any) => {
    const sessionCode = localStorage.getItem("sessionCode") ?? "";
    try {
      await fetch(`${apiBaseUrl}/blogs`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: sessionCode,
        },
        method,
        body: JSON.stringify({ ...values, _id: blog?._id?.toString() }),
      });
    } catch (err) {}
  };

  const handleDelete = async () => {
    setDeleteBlogState("loading");
    const sessionCode = localStorage.getItem("sessionCode") ?? "";
    try {
      await fetch(`${apiBaseUrl}/blogs?_id=${query.id}`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          Authorization: sessionCode,
        },
        method: "DELETE",
      });
      replace("/", "/", { locale });
    } catch (err) {
    } finally {
      setDeleteBlogState(null);
    }
  };

  return (
    <>
      <Formik
        initialValues={blog ?? initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting, values }) => (
          <Form>
            <FormFields />
            <Box
              sx={{
                mt: "25px",
                display: "flex",
              }}
            >
              <Button
                disabled={isSubmitting}
                variant="contained"
                type="submit"
                sx={{
                  fontWeight: 600,
                  minWidth: "189px",
                  "& circle": { color: "rgba(0,0,0,0.9)" },
                }}
              >
                <Translate k="save" namespace="backoffice" />
              </Button>
              <Box sx={{ flexGrow: 1 }} />
              <Button
                onClick={() => setDeleteBlogState("open")}
                variant="contained"
                color="error"
                sx={{
                  fontWeight: 600,
                  minWidth: "189px",
                  "& circle": { color: "rgba(0,0,0,0.9)" },
                }}
              >
                <Translate k="delete" namespace="backoffice" />
              </Button>
            </Box>
          </Form>
        )}
      </Formik>

      {!!deleteBlogState && (
        <Dialog open={true}>
          <Box sx={{ p: "20px", width: "300px" }}>
            <Typography>
              <Translate k="deleteConfirmation" namespace="backoffice" />
            </Typography>
            <Box
              width={1}
              sx={{
                mt: "30px",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Button
                disabled={deleteBlogState === "loading"}
                onClick={() => setDeleteBlogState("open")}
                variant="contained"
                sx={{
                  p: "8px 14px",
                  fontWeight: 600,
                  minWidth: "110px",
                  "& circle": { color: "rgba(0,0,0,0.9)" },
                }}
              >
                <Translate k="cancel" namespace="backoffice" />
              </Button>

              <Button
                disabled={deleteBlogState === "loading"}
                onClick={() => handleDelete()}
                variant="contained"
                color="error"
                sx={{
                  p: "8px 14px",
                  minWidth: "110px",
                  fontWeight: 600,
                  "& circle": { color: "rgba(0,0,0,0.9)" },
                }}
              >
                <Translate k="delete" namespace="backoffice" />
              </Button>
            </Box>
          </Box>
        </Dialog>
      )}
    </>
  );
};

export default BlogForm;
