import React, { FC, ReactElement } from "react";
import TextField from "@mui/material/TextField";
import { useFormikContext, getIn } from "formik";
import { useTranslation } from "next-i18next";
import { InitialValues, initialValues } from "./BlogForm";
import { Grid, Typography } from "@mui/material";
import Translate from "../Translate";
import { Editor } from "@tinymce/tinymce-react";
import { DynamicContent } from "../../lib/api/blog";

interface FormFieldsProps {}

const FormFields: FC<FormFieldsProps> = (): ReactElement => {
  const fields = Object.keys(initialValues);

  const { setFieldValue, values } = useFormikContext<InitialValues>();

  const onEditorInputChange = (newValue: string, name: string) => {
    setFieldValue(name, newValue, false);
  };

  return (
    <Grid
      container
      rowSpacing={5}
      columnSpacing={{ md: 5, xs: 0 }}
      sx={{
        py: "60px",
      }}
    >
      <>
        {fields.map((name) => {
          if (typeof initialValues[name] === "string") {
            return <InputField key={name} name={name} />;
          } else {
            const localizedValues = values[name] as DynamicContent;
            return (
              <React.Fragment key={name}>
                <Typography variant="h4" pl="20px" mt="30px" width={1}>
                  <Translate k={name} namespace="backoffice" />
                </Typography>
                {Object.keys(initialValues[name]).map((childName) => {
                  if (childName.includes("content")) {
                    return (
                      <Grid item key={`${name}.${childName}`} xs={12}>
                        <Editor
                          init={{
                            plugins:
                              "fullscreen image lists link media table anchor autolink codesample contextmenu hr pagebreak nonbreaking anchor insertdatetime advlist textcolor wordcount imagetools",
                            toolbar:
                              "undo redo | styles | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | bullist numlist | link image",
                            style_formats: [
                              {
                                title: "Headings",
                                items: [
                                  { title: "Heading 1", format: "h1" },
                                  { title: "Heading 2", format: "h2" },
                                  { title: "Heading 3", format: "h3" },
                                  { title: "Heading 4", format: "h4" },
                                  { title: "Heading 5", format: "h5" },
                                  { title: "Heading 6", format: "h6" },
                                ],
                              },
                              {
                                title: "Inline",
                                items: [
                                  { title: "Bold", inline: "b" },
                                  { title: "Italic", inline: "i" },
                                  { title: "Underline", inline: "u" },
                                  { title: "Strikethrough", inline: "strike" },
                                  { title: "Superscript", inline: "sup" },
                                  { title: "Subscript", inline: "sub" },
                                  { title: "Code", inline: "code" },
                                ],
                              },
                              {
                                title: "Blocks",
                                items: [
                                  { title: "Paragraph", format: "p" },
                                  { title: "Blockquote", format: "blockquote" },
                                  { title: "Div", format: "div" },
                                  { title: "Pre", format: "pre" },
                                ],
                              },
                            ],
                          }}
                          onEditorChange={(newValue) =>
                            onEditorInputChange(
                              newValue,
                              `${name}.${childName}`
                            )
                          }
                          value={localizedValues["content"] as string}
                          apiKey={process.env.NEXT_PUBLIC_TINY_API_KEY}
                        />
                      </Grid>
                    );
                  }
                  return (
                    <InputField
                      key={`${name}.${childName}`}
                      name={`${name}.${childName}`}
                    />
                  );
                })}
              </React.Fragment>
            );
          }
        })}
      </>
    </Grid>
  );
};

const InputField = ({ name }: { name: string }) => {
  const { touched, errors, getFieldProps } = useFormikContext<InitialValues>();
  const { t } = useTranslation("backoffice");

  const getErrorMessage = (name: string) => {
    if (getIn(touched, name) && getIn(errors, name)) {
      return t(getIn(errors, `${name}`));
    }
    return "";
  };

  const label = name.split(".").reverse();

  return (
    <Grid item key={name} xs={12} md={6}>
      <TextField
        label={t(label)}
        fullWidth
        {...getFieldProps(name)}
        error={!!(getIn(touched, name) && getIn(errors, name))}
        helperText={getErrorMessage(name)}
        multiline
        rows={name.includes("content") ? 7 : 1}
        sx={{
          "> div": {
            borderRadius: "10px",
            backgroundColor: "#fff",
          },
          "& p": {
            fontWeight: 500,
            textAlign: "right",
            fontSize: "12px",
            lineHeight: "16px",
            letterSpacing: "0.2px",
          },
        }}
      />
    </Grid>
  );
};

export default FormFields;
