import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Translate from "./Translate";
import Link from "next/link";

interface ContactUsButtonProps {
  small?: boolean;
}

const ContactUsButton: FC<ContactUsButtonProps> = ({ small }): ReactElement => {
  return (
    <Box>
      <Link href="/#contact" scroll={false}>
        <Button
          sx={(theme) => ({
            p: small ? "8px 24px" : "",
            zIndex: 101,
            position: "relative",
            flexShrink: 0,
            mr: "60px",
            [theme.breakpoints.down("md")]: {
              mr: "0px",
            },
          })}
        >
          <Translate k="contactUs" />
        </Button>
      </Link>
    </Box>
  );
};

export default ContactUsButton;
