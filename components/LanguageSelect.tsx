import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { MenuItem, TextField, Typography } from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { useRouter } from "next/router";

const languages = [
  { label: "SRB", lng: "sr" },
  { label: "ENG", lng: "en" },
];

interface LanguageSelectProps {}

const LanguageSelect: FC<LanguageSelectProps> = (): ReactElement => {
  const { locale, push } = useRouter();

  return (
    <Box>
      <TextField
        aria-label="Language"
        select
        variant="standard"
        value={locale}
        sx={{
          mt: "4px",
          "div, svg": {
            color: "#fff",
            background: "transparent",
          },
          "div:after": {
            border: "none",
          },
        }}
        SelectProps={{
          onChange: (e) => push("/", "/", { locale: e.target.value as string }),
          MenuProps: { disableScrollLock: true },
          disableUnderline: true,
          IconComponent: KeyboardArrowDownIcon,
          renderValue: (value) => (
            <Typography color="common.white">
              {languages.find((item) => item.lng === value)?.label}
            </Typography>
          ),
        }}
      >
        {languages.map(({ label, lng }) => (
          <MenuItem key={label} value={lng}>
            {label}
          </MenuItem>
        ))}
      </TextField>
    </Box>
  );
};

export default LanguageSelect;
