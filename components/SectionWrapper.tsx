import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { SxProps, Theme } from "@mui/material/styles";

interface SectionWrapperProps {
  id?: string;
  children: React.ReactElement;
  sx?: SxProps<Theme>;
}

const SectionWrapper: FC<SectionWrapperProps> = ({
  children,
  id,
  sx = {},
}): ReactElement => {
  return (
    <Box
      id={id}
      width={1}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        px: "30px",
        maxWidth: "100wv",
      }}
    >
      <Box
        width={1}
        maxWidth="1280px"
        sx={{
          display: "flex",
          ...sx,
        }}
      >
        {children}
      </Box>
    </Box>
  );
};

export default SectionWrapper;
