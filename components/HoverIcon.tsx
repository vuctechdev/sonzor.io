import React, { FC, ReactElement, useRef, useState } from "react";
import Box from "@mui/material/Box";
import TempIcon from "public/images/temperature_icon.svg";
import SunIcon from "public/images/sun_icon.svg";
import DropIcon from "public/images/raindrop_icon.svg";
import GrowthIcon from "public/images/growth_icon.svg";
import { Typography } from "@mui/material";
import Translate from "./Translate";
import Image from "next/image";

interface HoverIconProps {
  label: "heroGrowth" | "heroDrop" | "heroSun" | "heroTemp";
}

const icons = {
  heroGrowth: GrowthIcon,
  heroDrop: DropIcon,
  heroSun: SunIcon,
  heroTemp: TempIcon,
};

const getIconPosition = (key: string) => {
  const position: Record<string, any> = {
    heroGrowth: {
      top: "70%",
      left: "65%",
    },
    heroDrop: {
      top: "13%",
      left: "66%",
    },
    heroSun: {
      top: "91%",
      left: "13%",
    },
    heroTemp: {
      top: "46%",
      left: "-3%",
    },
  };

  return position[key];
};

const HoverIcon: FC<HoverIconProps> = ({ label }): ReactElement => {
  const ref = useRef<HTMLDivElement>();
  const [width, setWidth] = useState("59px");

  const expandDiv = () => {
    if (ref.current) {
      setWidth(`${ref.current.scrollWidth}px`);
    }
  };

  const resetDiv = () => {
    setWidth("59px");
  };

  const whiteIconBG = label === "heroDrop" || label === "heroTemp";

  return (
    <Box
      ref={ref}
      sx={(theme) => ({
        zIndex: 1000,
        position: "absolute",
        border: "6px solid #fff",
        borderRadius: "94.5px",
        width,
        height: "55px",
        transition: "width 0.7s ease",
        p: "6px 12px 6px 6px",
        display: "flex",
        alignItems: "center",
        overflow: "hidden",
        backgroundColor: whiteIconBG
          ? theme.palette.common.black
          : theme.palette.secondary.dark,
        ...getIconPosition(label),
        [theme.breakpoints.down("md")]: {
          height: "30px",
          borderWidth: "2px",
          p: "3px 8px 3px 3px",
          borderRadius: "51px",
          width: "fit-content",
        },
        [theme.breakpoints.down("sm")]: {
          minWidth: "120px",
        },
      })}
      onMouseEnter={expandDiv}
      onMouseLeave={resetDiv}
    >
      <Box
        sx={(theme) => ({
          backgroundColor: whiteIconBG
            ? theme.palette.common.white
            : theme.palette.common.black,
          borderRadius: "94.5px",
          width: "35px",
          height: "35px",
          flexShrink: 0,
          mr: "8px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          [theme.breakpoints.down("md")]: {
            width: "18.7px",
            height: "18.7px",
            mr: "4px",
          },
        })}
      >
        <Box
          sx={(theme) => ({
            width: "24px",
            height: "24px",
            position: "relative",
            [theme.breakpoints.down("md")]: {
              width: "12px",
              height: "12px",
            },
          })}
        >
          <Image src={icons[label]} alt={label} fill />
        </Box>
      </Box>
      <Box
        sx={(theme) => ({
          display: "flex",
          flexShrink: 0,
          pr: "12px",
          maxWidth: label === "heroDrop" ? "170px" : "unset",
          [theme.breakpoints.down("sm")]: {
            pr: "0px",
            maxWidth: label === "heroDrop" ? "85px" : "unset",
          },
        })}
      >
        <Typography variant="body3">
          <Translate k={label} />
        </Typography>
      </Box>
    </Box>
  );
};

export default HoverIcon;
