import React, { FC, ReactElement } from "react";
import Head from "next/head";
import Script from "next/script";
import { GTM_ID, appleTouchIcon, favicon, webURL } from "../lib/consts";

interface PageHeadProps {
  title: string;
  description: string;
  ogImage: string;
  canonicalURL?: string;
}

const PageHead: FC<PageHeadProps> = ({
  title,
  description,
  ogImage,
  canonicalURL,
}): ReactElement => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="icon" href={favicon} />
      <link rel="canonical" href={canonicalURL ?? webURL} />

      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:type" content="website" />
      <meta property="og:url" content={webURL} />
      <meta property="og:image" content={ogImage} />
      <meta property="og:image:type" content="image/jpeg" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />
      <meta property="og:image:alt" content={title} />

      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content={webURL} />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={description} />
      <meta property="twitter:image" content={ogImage} />

      <link rel="apple-touch-icon" href={appleTouchIcon} />
      {GTM_ID && (
        <Script id="google-tag-manager" strategy="afterInteractive">
          {`
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','${GTM_ID}');
      `}
        </Script>
      )}
    </Head>
  );
};

export default PageHead;
