import "../styles/globals.css";
import type { AppProps } from "next/app";
import ThemeProvider from "../styles/Theme";
import { ParallaxProvider } from "react-scroll-parallax";
import { appWithTranslation } from "next-i18next";

function App({ Component, pageProps }: AppProps) {
  return (
    <ParallaxProvider>
      <ThemeProvider>
        <Component {...pageProps} />
      </ThemeProvider>
    </ParallaxProvider>
  );
}

export default appWithTranslation(App);
