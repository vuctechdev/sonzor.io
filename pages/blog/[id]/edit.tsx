import React, { FC, ReactElement } from "react";
import Typography from "@mui/material/Typography";
import BlogForm from "../../../components/blog-form/BlogForm";
import { BlogType, getBlogs } from "../../../lib/api/blog";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useRouter } from "next/router";
import SectionWrapper from "../../../components/SectionWrapper";
import Translate from "../../../components/Translate";
import SessionGuard from "../../../components/SessionGuard";

interface BlogEditProps {
  blog: BlogType;
}

const BlogEdit: FC<BlogEditProps> = ({ blog }): ReactElement => {
  const router = useRouter();
  if (router.isFallback) return <div>Loading...</div>;
  return (
    <SectionWrapper sx={{ flexDirection: "column", py: "30px" }}>
      <SessionGuard>
        <>
          <Typography variant="h2">
            <Translate k="updateBlog" namespace="backoffice" />
          </Typography>
          <BlogForm blog={blog} />
        </>
      </SessionGuard>
    </SectionWrapper>
  );
};

export default BlogEdit;

export const getStaticPaths = async () => {
  const blogs = await getBlogs();
  return {
    paths: blogs.map((item) => ({ params: { id: item._id.toString() } })),
    fallback: true,
  };
};

export const getStaticProps = async ({
  params,
  locale,
}: {
  params: any;
  locale: string;
}) => {
  const blogs = await getBlogs();
  return {
    props: {
      blog: blogs.find((item) => item._id.toString() === params.id),
      ...(await serverSideTranslations(locale, ["backoffice"])),
    },
    revalidate: 20,
  };
};
