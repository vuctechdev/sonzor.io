import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { LocalizedBlogType, getLocalizedBlogs } from "../../../lib/api/blog";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import SectionWrapper from "../../../components/SectionWrapper";
import { Typography } from "@mui/material";
import Image from "next/image";
import { useRouter } from "next/router";
import PageHead from "../../../components/PageHead";
import parse from "html-react-parser";

interface BlogPageProps {
  blog: LocalizedBlogType;
}

const getDisplayDate = (date: string): string => {
  const d = new Date(date);
  return `${d.getDate()}.${d.getMonth() + 1}.${d.getFullYear()}.`;
};

const BlogPage: FC<BlogPageProps> = ({ blog }): ReactElement => {
  const router = useRouter();
  if (router.isFallback) return <div>Loading...</div>;

  const { title, subtitle, src, srcMin, content, createdAt } = blog;
  return (
    <>
      <PageHead title={title} description={subtitle} ogImage={src} />
      <main>
        <SectionWrapper>
          <Box
            width={1}
            sx={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Box
              width={1}
              sx={(theme) => ({
                height: "506px",
                position: "relative",
                mb: "30px",
                [theme.breakpoints.down("sm")]: {
                  height: "200px",
                },
              })}
            >
              <picture style={{ zIndex: 2 }}>
                <source srcSet={srcMin} media="(max-width: 420px)" />
                <Image
                  alt={title}
                  priority={true}
                  src={src}
                  fill
                  loading="eager"
                  style={{
                    objectFit: "cover",
                  }}
                />
              </picture>
            </Box>
            <Typography
              sx={{ mb: "20px" }}
              variant="body2"
              textAlign="right"
              color="primary"
            >
              {getDisplayDate(createdAt)}
            </Typography>
            <Typography variant="h1">{title}</Typography> <br />
            <Box
              width={1}
              sx={{
                mb: "40px",
              }}
            >
              <Typography variant="h4">{subtitle}</Typography>
            </Box>
            <Box width={1}>{parse(content)}</Box>
          </Box>
        </SectionWrapper>
      </main>
    </>
  );
};

export const getStaticPaths = async ({ locale }: { locale: string }) => {
  const blogs = await getLocalizedBlogs(locale);
  return {
    paths: blogs.map((item) => ({ params: { id: item._id.toString() } })),
    fallback: true,
  };
};

export const getStaticProps = async ({
  params,
  locale,
}: {
  params: any;
  locale: string;
}) => {
  const blogs = await getLocalizedBlogs(locale);
  return {
    props: {
      blog: blogs.find((item) => item._id.toString() === params.id),
      ...(await serverSideTranslations(locale)),
    },
    revalidate: 30,
  };
};

export default BlogPage;
