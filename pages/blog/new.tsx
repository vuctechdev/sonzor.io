import React, { FC, ReactElement } from "react";
import BlogForm from "../../components/blog-form/BlogForm";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Typography } from "@mui/material";
import SectionWrapper from "../../components/SectionWrapper";
import Translate from "../../components/Translate";
import SessionGuard from "../../components/SessionGuard";

interface NewBlogProps {}

const NewBlog: FC<NewBlogProps> = (): ReactElement => {
  return (
    <SectionWrapper sx={{ flexDirection: "column", py: "30px" }}>
      <SessionGuard>
        <>
          <Typography variant="h2" width={1}>
            <Translate k="createBlog" namespace="backoffice" />
          </Typography>
          <BlogForm />
        </>
      </SessionGuard>
    </SectionWrapper>
  );
};

export const getStaticProps = async ({ locale }: { locale: string }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["backoffice"])),
    },
    revalidate: 20,
  };
};

export default NewBlog;
