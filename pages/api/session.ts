import type { NextApiRequest, NextApiResponse } from "next";
import { sendSessionMail } from "../../lib/api/nodeMailer";
import { validateSession } from "../../lib/api/session";

type Data = {
  ok: boolean;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const recipients = process.env.NEXT_PUBLIC_EMAIL_RECIPIENTS ?? "";
  if (req.method === "GET") {
    const selectedEmail = (req.query.email as string) ?? "";
    if (recipients.split(",").includes(selectedEmail.trim())) {
      await sendSessionMail(selectedEmail.trim());
      return res.status(200).json({ ok: true });
    } else {
      return res.status(403).json({ ok: false });
    }
  } else if (req.method === "POST") {
    const sessionCode = req.body.sessionCode;
    if (validateSession(sessionCode)) {
      return res.status(200).json({ ok: true });
    }
    return res.status(401).json({ ok: false });
  } else {
    res.status(200).json({ ok: false });
  }
}
