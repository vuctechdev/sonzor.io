import type { NextApiRequest, NextApiResponse } from "next";
import Blog, {
  createNewBlog,
  updateBlog,
  deleteBlog,
} from "../../lib/api/blog";
import { connectDB } from "../../lib/api/db";
import { validateSession } from "../../lib/api/session";

type Data = {
  data: any;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  await connectDB();
  const sessionCode: string = (req.headers.authorization as string) ?? "";

  if (req.method === "POST") {
    if (!validateSession(sessionCode)) {
      return res.status(401).json({ data: "unauthorized" });
    }
    const newBlog = await createNewBlog(req.body);
    res.json({ data: newBlog });
  } else if (req.method === "PATCH") {
    if (!validateSession(sessionCode)) {
      return res.status(401).json({ data: "unauthorized" });
    }
    const updatedBlog = await updateBlog(req.body);
    res.json({ data: updatedBlog });
  } else if (req.method === "DELETE") {
    const _id = req.query._id;
    if (!validateSession(sessionCode) || !_id) {
      return res.status(401).json({ data: "unauthorized" });
    }
    await deleteBlog(_id as string);
    res.json({ data: true });
  } else {
    const data = await Blog.find();
    res.json({ data });
  }
}
