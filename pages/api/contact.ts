import type { NextApiRequest, NextApiResponse } from "next";
import { sendContactMail } from "../../lib/api/nodeMailer";

type Data = {
  ok: boolean;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === "POST") {
    await sendContactMail(req.body);
    res.status(200).json({ ok: true });
  } else {
    res.status(403).json({ ok: false });
  }
}
