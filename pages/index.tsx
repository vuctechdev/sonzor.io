import Hero from "../sections/Hero";
import Services from "../sections/Services";
import PageHead from "../components/PageHead";
import Navigation from "../sections/header";
import { FC, ReactElement } from "react";
import About from "../sections/About";
import SubHero from "../sections/SubHero";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import CTA from "../sections/CTA";
import Savings from "../sections/Savings";
import BottomWrapper from "../sections/bottom-wrapper";
import { LocalizedBlogType, getLocalizedBlogs } from "../lib/api/blog";
import Blogs from "../sections/Blogs";
import { useRouter } from "next/router";
import { descriptions, ogImage, title } from "../lib/consts";

type Props = {
  blogs: LocalizedBlogType[];
};

const Home: FC<Props> = ({ blogs }): ReactElement => {
  const { locale } = useRouter();
  const description = descriptions[locale as string];
  return (
    <>
      <PageHead title={title} description={description} ogImage={ogImage} />
      <main>
        <Navigation />
        <Hero />
        <SubHero />
        <CTA />
        <About />
        <Savings />
        <Services />
        <Blogs blogs={blogs} />
        <BottomWrapper />
      </main>
    </>
  );
};

export default Home;

export async function getStaticProps({ locale }: { locale: string }) {
  const blogs = await getLocalizedBlogs(locale);

  return {
    props: {
      blogs,
      ...(await serverSideTranslations(locale)),
    },
    revalidate: 30,
  };
}
