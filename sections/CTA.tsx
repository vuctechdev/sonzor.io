import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../components/SectionWrapper";
import { Typography } from "@mui/material";
import Translate from "../components/Translate";

interface CTAProps {}

const CTA: FC<CTAProps> = (): ReactElement => {
  return (
    <SectionWrapper>
      <Box
        width={1}
        sx={(theme) => ({
          display: "flex",
          justifyContent: "space-between",
          mt: "144px",
          [theme.breakpoints.down("md")]: {
            mt: "74px",
            flexDirection: "column",
          },
        })}
      >
        <Box
          sx={(theme) => ({
            width: "620px",
            [theme.breakpoints.down("md")]: {
              width: "100%",
            },
          })}
        >
          <Typography variant="h2">
            <Translate k="ctaTitle" />
          </Typography>
        </Box>
        <Box
          sx={(theme) => ({
            width: "510px",
            [theme.breakpoints.down("md")]: {
              width: "100%",
              mt: "24px",
            },
          })}
        >
          <Typography>
            <Translate k="ctaContent" />
          </Typography>
        </Box>
      </Box>
    </SectionWrapper>
  );
};

export default CTA;
