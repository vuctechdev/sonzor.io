import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import ContactUsButton from "../../components/ContactUsButton";
import Translate from "../../components/Translate";
import LanguageSelect from "../../components/LanguageSelect";
import Link from "next/link";

interface DesktopNavigationProps {
  sections: { id: string }[];
}

const DesktopNavigation: FC<DesktopNavigationProps> = ({
  sections,
}): ReactElement => {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        {sections.map((section) => (
          <Box
            key={section.id}
            sx={(theme) => ({
              mr: "60px",
              "& a": {
                textDecoration: "none",
              },
              [theme.breakpoints.down("sm")]: {
                fontSize: "14px",
                margin: "0px 10px",
              },
            })}
          >
            <Link href={`/#${section.id}`} scroll={false}>
              <Typography color="common.white">
                <Translate k={section.id} />
              </Typography>
            </Link>
          </Box>
        ))}
      </Box>
      <ContactUsButton small />
      <LanguageSelect />
    </>
  );
};

export default DesktopNavigation;
