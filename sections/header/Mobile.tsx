import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { Dialog, IconButton, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import ContactUsButton from "../../components/ContactUsButton";
import Translate from "../../components/Translate";
import LanguageSelect from "../../components/LanguageSelect";
import Link from "next/link";

interface MobileNavigationProps {
  sections: { id: string }[];
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const MobileNavigation: FC<MobileNavigationProps> = ({
  sections,
  setOpen,
}): ReactElement => {
  return (
    <Dialog open={true} fullScreen>
      <IconButton
        size="large"
        onClick={() => setOpen(false)}
        sx={{
          position: "absolute",
          left: "16px",
          top: "6px",
          color: "#fff",
        }}
      >
        <CloseIcon fontSize="large" />
      </IconButton>
      <Box
        height={1}
        sx={(theme) => ({
          background:
            "linear-gradient(180deg, #24352B 0%, #38493F 37.5%, #13281F 100%)",
          display: "flex",
          flexDirection: "column",
          p: "90px 0px 50px",
        })}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
            flexGrow: 1,
            pr: "30px",
          }}
        >
          {sections.map((section) => (
            <Box
              key={section.id}
              onClick={() => setOpen(false)}
              sx={(theme) => ({
                mb: "32px",
                // mr: "30px",
                "& a": {
                  textDecoration: "none",
                },
              })}
            >
              <Link href={`/#${section.id}`} scroll={false}>
                <Typography variant="h1" color="common.white" textAlign="right">
                  <Translate k={section.id} />
                </Typography>
              </Link>
            </Box>
          ))}
          <LanguageSelect />
        </Box>
        <ContactUsButton small />
      </Box>
    </Dialog>
  );
};

export default MobileNavigation;
