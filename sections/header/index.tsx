import Box from "@mui/material/Box";
import React, { FC, ReactElement, useState } from "react";
import { Hidden, IconButton } from "@mui/material";
import dynamic from "next/dynamic";
import MenuIcon from "@mui/icons-material/Menu";
import SectionWrapper from "../../components/SectionWrapper";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import LogoIMG from "public/images/logo.png";

const DesktopNavigation = dynamic(() => import("./Desktop"));
const MobileNavigation = dynamic(() => import("./Mobile"));

export const navLinks = [
  {
    id: "savings",
  },
  {
    id: "blog",
  },
];

const Navigation: FC = (): ReactElement => {
  const { locale } = useRouter();
  const [open, setOpen] = useState(false);
  return (
    <Box
      width={1}
      sx={(theme) => ({
        py: "20px",
        position: "absolute",
        top: "0px",
        [theme.breakpoints.down("md")]: {
          pt: "38px",
          pb: "0px",
        },
      })}
    >
      <SectionWrapper>
        <>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
              flexGrow: 1,
            }}
          >
            <Link href="/" locale={locale}>
              <Box
                sx={{
                  position: "relative",
                  width: "183px",
                  height: "36px",
                }}
              >
                <Image
                  alt="Logo"
                  priority={true}
                  src={LogoIMG}
                  fill
                  quality={1}
                  loading="eager"
                />
              </Box>
            </Link>
          </Box>
          <Hidden mdDown>
            <DesktopNavigation sections={navLinks} />
          </Hidden>
          <Hidden mdUp>
            <Box width={1} sx={{ display: "flex", justifyContent: "flex-end" }}>
              <IconButton
                onClick={() => setOpen(true)}
                sx={(theme) => ({
                  color: theme.palette.common.white,
                  padding: 0,
                })}
                aria-label="Menu"
              >
                <MenuIcon fontSize="large" />
              </IconButton>
            </Box>

            {open && <MobileNavigation setOpen={setOpen} sections={navLinks} />}
          </Hidden>
        </>
      </SectionWrapper>
    </Box>
  );
};

export default Navigation;
