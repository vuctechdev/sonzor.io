import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../components/SectionWrapper";
import Typography from "@mui/material/Typography";
import Translate from "../components/Translate";
import Image from "next/image";
import ContactUsButton from "../components/ContactUsButton";
import HeroMinIMG from "public/images/hero_min.webp";
import HoverIcon from "../components/HoverIcon";
import { sm } from "../styles/Theme";

const Hero: FC = (): ReactElement => {
  return (
    <Box
      sx={(theme) => ({
        borderRadius: "0px 0px 64px 64px",
        background:
          "linear-gradient(180deg, #24352B 0%, #38493F 37.5%, #13281F 100%)",
        [theme.breakpoints.down("sm")]: {
          borderRadius: "0px 0px 40px 40px",
        },
      })}
    >
      <SectionWrapper>
        <Box
          width={1}
          sx={(theme) => ({
            justifyContent: "space-between",
            height: "845px",
            pt: "220px",
            pb: "170px",
            display: "flex",
            alignItems: "center",
            [theme.breakpoints.down("md")]: {
              flexDirection: "column",
              justifyContent: "unset",
              height: "auto",
              pt: "127px",
              pb: "98px",
            },
          })}
        >
          <Box
            sx={(theme) => ({
              width: "510px",
              mr: "20px",
              [theme.breakpoints.down("md")]: {
                width: "100%",
                mr: "0px",
              },
            })}
          >
            <Typography variant="h2" color="common.white">
              <Translate k="heroTitle" />
            </Typography>
            <Typography
              color="common.white"
              sx={(theme) => ({
                mt: "24px",
                mb: "36px",
              })}
            >
              <Translate k="heroContent" />
            </Typography>
            <Box
              textAlign="left"
              sx={(theme) => ({
                mb: "0px",
                [theme.breakpoints.down("md")]: {
                  mb: "63px",
                },
              })}
            >
              <ContactUsButton />
            </Box>
          </Box>
          <Box
            sx={(theme) => ({
              position: "relative",
              width: "635px",
              height: "430px",
              display: "flex",
              alignItems: "center",
              [theme.breakpoints.down("sm")]: {
                width: "100%",
                maxWidth: "380px",
                height: "auto",
              },
            })}
          >
            <Box
              sx={(theme) => ({
                position: "relative",
                // width: "635px",
                // height: "430px",
                // display: "flex",
                // alignItems: "center",
                // [theme.breakpoints.down("sm")]: {
                //   width: "100%",
                //   maxWidth: "380px",
                //   height: "auto",
                // },
              })}
            >
              <HoverIcon label="heroDrop" />
              <HoverIcon label="heroGrowth" />
              <HoverIcon label="heroSun" />
              <HoverIcon label="heroTemp" />
              <picture style={{ zIndex: 2 }}>
                <source
                  srcSet="/images/hero.webp"
                  media={`(min-width: ${sm}px)`}
                />
                <Image
                  alt="Hero"
                  priority={true}
                  src={HeroMinIMG}
                  loading="eager"
                  sizes="100vw"
                  style={{
                    width: "100%",
                    height: "auto",
                  }}
                />
              </picture>
            </Box>
          </Box>
        </Box>
      </SectionWrapper>
    </Box>
  );
};

export default Hero;
