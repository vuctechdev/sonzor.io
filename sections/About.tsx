import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../components/SectionWrapper";
import { Typography } from "@mui/material";
import Translate from "../components/Translate";
import Image from "next/image";
import { sm } from "../styles/Theme";

interface AboutProps {}

const data = [
  {
    title: "sensors",
    src: "about1",
  },
  {
    title: "dataProcess",
    src: "about2",
  },
  {
    title: "mobileApp",
    src: "about3",
  },
];

const About: FC<AboutProps> = (): ReactElement => {
  return (
    <SectionWrapper sx={{}}>
      <Box
        width={1}
        sx={(theme) => ({
          position: "sticky",

          // backgroundAttachment: "fixed",
          mt: "160px",
          display: "flex",
          justifyContent: "space-between",
          [theme.breakpoints.down("lg")]: {
            mt: "87px",
            flexDirection: "column",
          },
        })}
      >
        <Box
          sx={(theme) => ({
            width: "290px",
            [theme.breakpoints.down("lg")]: {
              width: "100%",
              mb: "47px",
            },
          })}
        >
          <Typography variant="h2">
            <Translate k="aboutTitle" />
          </Typography>
        </Box>
        <Box
          sx={(theme) => ({
            display: "flex",
            columnGap: "12px",
            [theme.breakpoints.down("lg")]: {
              justifyContent: "center",
              flexWrap: "wrap",
              rowGap: "12px",
            },
            [theme.breakpoints.down("md")]: {
              // flexDirection: "column",
              alignItems: "center",
              // columnGap: "0px",
            },
          })}
        >
          {data.map((item, i) => (
            <a
              href={`/#service${i + 1}`}
              key={item.title}
              style={{ textDecoration: "none" }}
            >
              <AboutCard data={item} />
            </a>
          ))}
        </Box>
      </Box>
    </SectionWrapper>
  );
};

interface AboutCardProps {
  data: { title: string; src: string };
}

const AboutCard: FC<AboutCardProps> = ({
  data: { title, src },
}): ReactElement => (
  <Box
    sx={(theme) => ({
      height: "573px",
      width: "286px",
      borderRadius: "40px",
      p: "12px",
      backgroundColor: theme.palette.secondary.main,
      [theme.breakpoints.down("sm")]: {
        height: "276px",
        width: "332px",
        p: "8px",
        mb: "16px",
        // width: "100%",
        // aspectRatio: "276/332",
      },
    })}
  >
    <Box
      sx={(theme) => ({
        height: "431px",
        width: "262px",
        // width: "100%",
        borderRadius: "28px",
        overflow: "hidden",
        position: "relative",
        [theme.breakpoints.down("sm")]: {
          // width: "100%",
          // aspectRatio: "160/315",
          height: "160px",
          width: "315px",
          p: "8px",
        },
      })}
    >
      <picture>
        <source srcSet={`/images/${src}.webp`} media={`(min-width: ${sm}px)`} />
        <Image
          alt={title}
          src={`/images/${src}_min.webp`}
          fill
          loading="lazy"
        />
      </picture>
    </Box>
    <Typography
      variant="h3"
      sx={(theme) => ({
        mt: "24px",
        ml: "8px",
        [theme.breakpoints.down("sm")]: {
          mt: "16px",
        },
      })}
    >
      <Translate k={title} />
    </Typography>
  </Box>
);

export default About;
