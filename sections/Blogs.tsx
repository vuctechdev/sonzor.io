import React, { FC, ReactElement, useState } from "react";
import Box from "@mui/material/Box";
import { LocalizedBlogType } from "../lib/api/blog";
import SectionWrapper from "../components/SectionWrapper";
import { Button, Typography } from "@mui/material";
import Translate from "../components/Translate";
import Image from "next/image";
import Link from "next/link";

interface BlogsProps {
  blogs: LocalizedBlogType[];
}

const Blogs: FC<BlogsProps> = ({ blogs }): ReactElement => {
  const [page, setPage] = useState(1);
  const itemsPerPage = 3;
  const displayLoadMore = page * itemsPerPage < blogs.length;
  return (
    <SectionWrapper>
      <Box
        width={1}
        id="blog"
        sx={(theme) => ({
          mt: "80px",
          [theme.breakpoints.down("sm")]: {
            mt: "40px",
          },
        })}
      >
        <Typography variant="h2" width={1}>
          <Translate k="blog" />{" "}
        </Typography>
        <Box
          width={1}
          sx={(theme) => ({
            mt: "46px",
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
            columnGap: "80px",
            rowGap: "25px",
            [theme.breakpoints.down("sm")]: {
              mt: "36px",
              rowGap: "39px",
              columnGap: "0px",
              flexDirection: "column",
              alignItems: "center",
            },
          })}
        >
          {blogs
            .reverse()
            .slice(0, itemsPerPage * page)
            .map((blog) => (
              <BlogItem key={blog._id} blog={blog} />
            ))}
        </Box>
        {displayLoadMore && (
          <Box
            width={1}
            sx={(theme) => ({
              mt: "60px",
              [theme.breakpoints.down("sm")]: {
                mt: "40px",
              },
            })}
          >
            <Button onClick={() => setPage((prev) => prev + 1)}>
              {" "}
              <Translate k="loadMore" />{" "}
            </Button>
          </Box>
        )}
      </Box>
    </SectionWrapper>
  );
};

interface BlogItemProps {
  blog: LocalizedBlogType;
}

const BlogItem: FC<BlogItemProps> = ({ blog }): ReactElement => {
  const { createdAt, _id, srcMin, title, subtitle } = blog;

  const day = 1000 * 60 * 60 * 24;
  const newBlog =
    new Date(createdAt).getTime() > new Date().getTime() - day * 30;

  return (
    <Link
      href={`/blog/${_id}`}
      style={{
        textDecoration: "none",
        width: "100%",
        maxWidth: "373px",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Box
        sx={(theme) => ({
          height: "450px",
          width: "373px",
          borderRadius: "40px",
          p: "20px 16px 30px 16px",
          backgroundColor: theme.palette.secondary.main,
          [theme.breakpoints.down("sm")]: {
            height: "auto",
            width: "100%",
            maxWidth: "333px",
          },
        })}
      >
        <Box
          width={1}
          sx={(theme) => ({
            height: "205px",
            width: "341px",
            borderRadius: "20px",
            overflow: "hidden",
            position: "relative",
            [theme.breakpoints.down("sm")]: {
              width: "100%",
            },
          })}
        >
          {newBlog && (
            <Box
              sx={(theme) => ({
                zIndex: 10,
                position: "absolute",
                left: "14px",
                top: "10px",
                backgroundColor: theme.palette.primary.main,
                p: "4px 16px",
                borderRadius: "100px",
              })}
            >
              <Typography>
                <Translate k="newBlog" />
              </Typography>
            </Box>
          )}
          <Image
            alt={title}
            src={srcMin}
            fill
            sizes="100vw"
            loading="lazy"
            style={{
              objectFit: "cover",
            }}
          />
        </Box>
        <Typography
          variant="h4"
          sx={{
            mt: "30px",
          }}
        >
          {title}
        </Typography>
        <Typography
          sx={{
            mt: "8px",
          }}
        >
          {subtitle}
        </Typography>
      </Box>
    </Link>
  );
};

export default Blogs;
