import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../components/SectionWrapper";
import Typography from "@mui/material/Typography";
import Translate from "../components/Translate";
import Image from "next/image";
import { Parallax } from "react-scroll-parallax";

const data = [
  {
    icon: "/images/savings1.svg",
    title: "savingsSubtitle1",
    content: "savingsContent1",
  },
  {
    icon: "/images/savings2.svg",
    title: "savingsSubtitle2",
    content: "savingsContent2",
  },
];

const Savings: FC = (): ReactElement => {
  return (
    <Box>
      <Parallax speed={30}>
        <Box
          width={1}
          zIndex={1000}
          sx={(theme) => ({
            backgroundColor: theme.palette.common.black,
            pt: "124px",
            pb: "156px",
            borderRadius: "64px",
            mt: "10px",
            [theme.breakpoints.down("md")]: {
              mt: "-30px",
              pt: "90px",
              pb: "90px",
              flexDirection: "column",
              borderRadius: "40px",
            },
          })}
        >
          <SectionWrapper>
            <Box
              width={1}
              sx={(theme) => ({
                display: "flex",
                justifyContent: "space-between",
                [theme.breakpoints.down("md")]: {
                  flexDirection: "column",
                },
              })}
            >
              <Box
                sx={(theme) => ({
                  width: "400px",
                  [theme.breakpoints.down("md")]: {
                    width: "100%",
                    mb: "64px",
                  },
                })}
              >
                <Typography variant="h2" color="common.white">
                  <Translate k="savingsTitle" />
                </Typography>
              </Box>
              <Box
                sx={(theme) => ({
                  display: "flex",
                  flexDirection: "column",
                  rowGap: "130px",
                  [theme.breakpoints.down("md")]: {
                    rowGap: "60px",
                  },
                })}
              >
                {data.map(({ icon, title, content }) => (
                  <Box
                    key={title}
                    sx={(theme) => ({
                      position: "relative",
                      display: "flex",
                      flexDirection: "column",
                      width: "570px",
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    })}
                  >
                    <Image
                      alt={title}
                      src={icon}
                      width="83"
                      height="83"
                      loading="lazy"
                    />
                    <Typography
                      variant="h4"
                      color="common.white"
                      sx={{
                        my: "30px",
                      }}
                    >
                      <Translate k={title} />
                    </Typography>
                    <Typography color="common.white">
                      <Translate k={content} />
                    </Typography>
                  </Box>
                ))}
              </Box>
            </Box>
          </SectionWrapper>
        </Box>
      </Parallax>
    </Box>
  );
};

export default Savings;
