import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import SectionWrapper from "../components/SectionWrapper";
import CountUp from "react-countup";
import Translate from "../components/Translate";

const data = [
  {
    value: 10,
    suffix: "+",
    desc: "Mernih vrednosti",
  },
  {
    value: 30,
    suffix: "%",
    desc: "Manje vode",
  },
  {
    value: 30,
    suffix: "%",
    desc: "Manje vode.",
  },
  {
    value: 3000,
    prefix: "$",
    desc: "Usteda",
  },
];

const SubHero: FC = (): ReactElement => {
  return (
    <Box sx={{ maxWidth: "100vw", overflowX: "auto" }} id="savings">
      <Box
        width={1}
        sx={(theme) => ({
          mt: "45px",
          minWidth: "836px",
          [theme.breakpoints.down("sm")]: { mt: "52px" },
        })}
      >
        <SectionWrapper
          sx={{
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <>
            {data.map(({ value, desc, suffix, prefix }) => (
              <Box
                key={desc}
                sx={(theme) => ({
                  display: "flex",
                  flexDirection: "column",
                  minWidth: "235px",
                  [theme.breakpoints.down("sm")]: { minWidth: "auto" },
                })}
              >
                <CountUp
                  start={value < 2000 ? 0 : 2500}
                  end={value}
                  duration={value < 2000 ? 4 : 3}
                  prefix={prefix}
                  suffix={suffix}
                  delay={0}
                >
                  {({ countUpRef }) => (
                    <Typography ref={countUpRef} variant="savingsMain" />
                  )}
                </CountUp>
                <Typography variant="savingsSub">
                  <Translate k={desc} />
                </Typography>
              </Box>
            ))}
          </>
        </SectionWrapper>
      </Box>
    </Box>
  );
};

export default SubHero;
