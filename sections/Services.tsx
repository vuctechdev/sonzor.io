import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../components/SectionWrapper";
import Typography from "@mui/material/Typography";
import Translate from "../components/Translate";
import Image from "next/image";
import Service1IMG from "public/images/services1.webp";
import Service2IMG from "public/images/services2.webp";
import Service3IMG from "public/images/services3.webp";
import { sm } from "../styles/Theme";

const data = [
  {
    title: "servicesTitle1",
    content: "servicesContent1",
    src: Service1IMG,
    srcMin: "images/services1_min.webp",
  },
  {
    title: "servicesTitle2",
    content: "servicesContent2",
    src: Service2IMG,
    srcMin: "images/services2_min.webp",
  },
  {
    title: "servicesTitle3",
    content: "servicesContent3",
    src: Service3IMG,
    srcMin: "images/services3_min.webp",
  },
];

const Services: FC = (): ReactElement => {
  return (
    <Box
      width={1}
      sx={(theme) => ({
        mt: "-70px",
        [theme.breakpoints.down("md")]: {
          mt: "-30px",
        },
      })}
    >
      <SectionWrapper sx={{ flexDirection: "column" }}>
        <>
          {data.map(({ title, content, src, srcMin }, i) => (
            <Box
              id={`service${i + 1}`}
              key={title}
              width={1}
              sx={(theme) => ({
                height: "626px",
                display: "flex",
                flexDirection: i % 2 === 1 ? "row-reverse" : "row",
                alignItems: "center",
                justifyContent: "space-between",
                columnGap: "40px",
                [theme.breakpoints.down("md")]: {
                  flexDirection: "column",
                  height: "auto",
                },
              })}
            >
              <Box
                // width={1}
                sx={(theme) => ({
                  position: "relative",
                  [theme.breakpoints.down("md")]: {},
                })}
              >
                <picture>
                  <source srcSet={srcMin} media={`(max-width: ${sm}px)`} />
                  <Image
                    alt={title}
                    src={src}
                    sizes="100vw"
                    loading="lazy"
                    style={{
                      width: "100%",
                      height: "auto",
                    }}
                  />
                </picture>
              </Box>
              <Box
                sx={(theme) => ({
                  width: "534px",
                  [theme.breakpoints.down("md")]: {
                    width: "100%",
                  },
                })}
              >
                <Typography
                  variant="h2"
                  sx={(theme) => ({
                    mb: "36px",
                    [theme.breakpoints.down("md")]: {
                      mt: "36px",
                    },
                  })}
                >
                  <Translate k={title} />
                </Typography>

                <Typography
                  sx={(theme) => ({
                    [theme.breakpoints.down("md")]: {
                      mb: "80px",
                    },
                  })}
                >
                  <Translate k={content} />
                </Typography>
              </Box>
            </Box>
          ))}
        </>
      </SectionWrapper>
    </Box>
  );
};

export default Services;
