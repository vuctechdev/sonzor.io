import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import ObserverLazyLoad from "../../lib/hooks/ObserverLazyLoad";
import dynamic from "next/dynamic";
import SectionWrapper from "../../components/SectionWrapper";
import Footer from "./Footer";

interface BottomWrapperProps {}

const Contact = dynamic(() => import("./contact/Contact"), {
  ssr: false,
});

const BottomWrapper: FC<BottomWrapperProps> = (): ReactElement => {
  const { ref, render } = ObserverLazyLoad();
  return (
    <Box
      id="contact"
      ref={ref}
      sx={(theme) => ({
        backgroundColor: theme.palette.common.black,
        minHeight: "1000px",
        borderRadius: "80px 80px 0px 0px",
        mt: "186px",
        pt: "97px",
        flexDirection: "column",
        display: "flex",
        justifyContent: "flex-end",
        [theme.breakpoints.down("md")]: {
          mt: "160px",
          pt: "60px",
          borderRadius: "40px 40px 0px 0px",
        },
      })}
    >
      {render && <Contact />}
      <SectionWrapper>
        <Box
          width={1}
          sx={(theme) => ({
            height: "1px",
            backgroundColor: theme.palette.secondary.main,
            opacity: 0.2,
            my: "100px",
            [theme.breakpoints.down("md")]: {
              my: "60px",
            },
          })}
        />
      </SectionWrapper>
      <Footer />
    </Box>
  );
};

export default BottomWrapper;
