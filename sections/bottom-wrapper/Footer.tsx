import React, { FC, ReactElement } from "react";
import Box from "@mui/material/Box";
import SectionWrapper from "../../components/SectionWrapper";
import LogoIMG from "../../public/images/logo.png";
import Image from "next/image";
import { Typography } from "@mui/material";
import Translate from "../../components/Translate";
import { navLinks } from "../header";
import FacebookIcon from "../../public/images/fb.svg";
import InstagramIcon from "../../public/images/ln.svg";
import LinkedinIcon from "../../public/images/in.svg";
import Link from "next/link";

const socialLinks = [
  { icon: LinkedinIcon, href: "" },
  { icon: FacebookIcon, href: "" },
  { icon: InstagramIcon, href: "/" },
];

const legalLinks = [
  { label: "Privacy Policy", href: "" },
  { label: "Terms of Use", href: "" },
  { label: "© 2024 Senzor.io", href: "" },
];

const Footer: FC = (): ReactElement => {
  return (
    <SectionWrapper>
      <Box
        width={1}
        sx={(theme) => ({
          pb: "130px",
          [theme.breakpoints.down("md")]: {
            pb: "60px",
          },
        })}
      >
        <Link href="/" scroll={false}>
          <Box
            sx={{
              position: "relative",
              width: "183px",
              height: "36px",
              mb: "40px",
            }}
          >
            <Image
              alt="Logo"
              priority={true}
              src={LogoIMG}
              fill
              quality={1}
              loading="eager"
            />
          </Box>
        </Link>
        <Box
          sx={(theme) => ({
            display: "flex",
            [theme.breakpoints.down("md")]: {
              flexDirection: "column-reverse",
            },
          })}
        >
          <Box
            sx={(theme) => ({
              display: "flex",
              [theme.breakpoints.down("md")]: {
                flexDirection: "column",
              },
            })}
          >
            <Box
              sx={(theme) => ({
                width: "285px",
                mr: "40px",
                [theme.breakpoints.down("md")]: {
                  mt: "30px",
                  mb: "40px",
                  mr: "0px",
                },
              })}
            >
              <Typography color="common.white">
                <Translate k="headOffice" />{" "}
              </Typography>
              <Typography color="common.white">
                385 Tremont Ave, East Orange, NJ 07018, United States
              </Typography>
            </Box>
            <Box>
              <Typography color="common.white">
                (604) 555-5555 <br />
                contact@aisolutions.com
              </Typography>
            </Box>
          </Box>

          <Box
            sx={(theme) => ({
              display: "flex",
              justifyContent: "flex-end",
              flexGrow: 1,
              [theme.breakpoints.down("md")]: {
                flexDirection: "column",
                rowGap: "20px",
              },
            })}
          >
            {navLinks.map(({ id }) => (
              <Link
                href={`/#${id}`}
                key={id}
                style={{ textDecoration: "none" }}
                scroll={false}
              >
                <Typography
                  color="common.white"
                  sx={(theme) => ({
                    ml: "56px",
                    [theme.breakpoints.down("md")]: {
                      ml: "0px",
                    },
                  })}
                >
                  <Translate k={id} />
                </Typography>
              </Link>
            ))}
          </Box>
        </Box>
        <Box
          sx={(theme) => ({
            display: "flex",
            mt: "40px",
            [theme.breakpoints.down("md")]: {
              flexDirection: "column",
              rowGap: "20px",
            },
          })}
        >
          <Box
            sx={{
              display: "flex",
              columnGap: "24px",
            }}
          >
            {socialLinks.map(({ icon, href }) => (
              <a key={href} href={href}>
                <Image alt="Social Icon" src={icon} />
              </a>
            ))}
          </Box>
          <Box
            sx={(theme) => ({
              display: "flex",
              justifyContent: "flex-end",
              flexGrow: 1,
              position: "relative",
              left: "50px",
              [theme.breakpoints.down("lg")]: {
                left: "0px",
              },
              [theme.breakpoints.down("md")]: {
                justifyContent: "flex-start",
                rowGap: "20px",
              },
            })}
          >
            {legalLinks.map(({ label, href }) => (
              <a href={href} key={label} style={{ textDecoration: "none" }}>
                <Typography
                  variant="body2"
                  color="common.white"
                  sx={(theme) => ({
                    ml: "56px",
                    [theme.breakpoints.down("md")]: {
                      ml: "0px",
                      mr: "10px",
                    },
                  })}
                >
                  <Translate k={label} />
                </Typography>
              </a>
            ))}
          </Box>
        </Box>
      </Box>
    </SectionWrapper>
  );
};

export default Footer;
