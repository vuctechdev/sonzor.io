import TextField from "@mui/material/TextField";
import { useFormikContext } from "formik";
import React, { ReactElement } from "react";
import { InitialValues, initialValues } from "./Contact";
import { useTranslation } from "next-i18next";

type Keys = "name" | "email" | "phone" | "message";

const FormFields = (): ReactElement => {
  const { touched, errors, getFieldProps } = useFormikContext<InitialValues>();
  const { t } = useTranslation();

  const getErrorMessage = (name: Keys) => {
    if (touched[name] && errors[name]) {
      return t(errors[name] as string);
    }
    return "";
  };

  const fields = Object.keys(initialValues) as Keys[];

  return (
    <>
      {fields.map((name) => (
        <TextField
          key={name}
          fullWidth
          {...getFieldProps(name)}
          placeholder={t(name)}
          error={touched[name] && !!errors[name]}
          helperText={getErrorMessage(name)}
          multiline
          rows={name === "message" ? 3.6 : 1}
          sx={{
            "> div": {
              borderRadius: "10px",
              backgroundColor: "#fff",
              border: "1px solid #A9B7C5",
            },
            "& p": {
              fontWeight: 500,
              textAlign: "right",
              fontSize: "12px",
              lineHeight: "16px",
              letterSpacing: "0.2px",
            },
          }}
        />
      ))}
    </>
  );
};

export default FormFields;
