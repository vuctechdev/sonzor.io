import React, { FC, ReactElement, useState } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Translate from "../../../components/Translate";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import FormFields from "./FormFields";
import CircularProgress from "@mui/material/CircularProgress";
import MarkEmailReadIcon from "@mui/icons-material/MarkEmailRead";
import { apiBaseUrl } from "../../../lib/consts";
import SectionWrapper from "../../../components/SectionWrapper";

export const initialValues = {
  name: "",
  email: "",
  phone: "",
  message: "",
};

export type InitialValues = typeof initialValues;

const validationSchema = Yup.object().shape({
  name: Yup.string().required("required"),
  email: Yup.string().email("invalidEmail").required("required"),
  phone: Yup.string().required("required"),
});

const Contact: FC = (): ReactElement => {
  const [sent, setSent] = useState(false);

  const handleSubmit = async (values: InitialValues) => {
    try {
      await fetch(`${apiBaseUrl}/contact`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        method: "POST",
        body: JSON.stringify(values),
      });
      setSent(true);
    } catch (err) {}
  };
  return (
    <SectionWrapper>
      <Box
        width={1}
        sx={(theme) => ({
          display: "flex",
          justifyContent: "space-between",
          columnGap: "40px",
          [theme.breakpoints.down("md")]: {
            columnGap: "0px",
            flexDirection: "column",
            alignItems: "center",
          },
        })}
      >
        <Box
          sx={(theme) => ({
            width: "560px",
            [theme.breakpoints.down("md")]: {
              width: "100%",
              mb: "60px",
            },
          })}
        >
          <Typography variant="h2" color="common.white">
            <Translate k="contactTitle" />{" "}
          </Typography>
        </Box>
        <Box
          sx={(theme) => ({
            backgroundColor: theme.palette.secondary.main,
            width: "539px",
            p: "40px",
            borderRadius: "40px",
            [theme.breakpoints.down("md")]: {
              width: "100%",
              maxWidth: "539px",
              p: "30px 16px",
              borderRadius: "20px",
            },
          })}
        >
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {({ isSubmitting }) => (
              <Form>
                <Grid
                  container
                  sx={(theme) => ({
                    rowGap: "25px",
                    [theme.breakpoints.down("md")]: {
                      rowGap: "25px",
                    },
                  })}
                >
                  <FormFields />
                </Grid>
                <Box
                  sx={{
                    mt: "25px",
                    display: "flex",
                  }}
                >
                  <Button
                    disabled={isSubmitting}
                    variant="contained"
                    type="submit"
                    color={sent ? "success" : "primary"}
                    sx={{
                      fontWeight: 600,
                      minWidth: "189px",
                      "& circle": { color: "rgba(0,0,0,0.9)" },
                    }}
                  >
                    {isSubmitting ? (
                      <CircularProgress size={28} />
                    ) : sent ? (
                      <MarkEmailReadIcon />
                    ) : (
                      <Translate k="sendMessage" />
                    )}
                  </Button>
                </Box>
              </Form>
            )}
          </Formik>
        </Box>
      </Box>
    </SectionWrapper>
  );
};

export default Contact;
