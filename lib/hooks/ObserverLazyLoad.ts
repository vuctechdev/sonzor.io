import { MutableRefObject, useEffect, useRef, useState } from "react";

interface ReturnType {
  ref: MutableRefObject<any>;
  render: boolean;
}

const ObserverLazyLoad = (): ReturnType => {
  const [render, setRender] = useState(false);
  const ref = useRef<any>(null);

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting && ref.current) {
          observer.unobserve(ref.current as unknown as Element);
          setRender(true);
        }
      });
    });

    const element = ref.current as unknown as Element | null;

    if (element) {
      observer.observe(element);
    }

    return () => {
      if (element) {
        observer.unobserve(element);
      }
    };
  }, [ref]);

  return { ref, render };
};

export default ObserverLazyLoad;
