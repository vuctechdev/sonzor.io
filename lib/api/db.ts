import mongoose from "mongoose";

const url = process.env.DB_CONNECTION ?? ""

let connection: typeof mongoose | null = null;
export const connectDB = async () => {
  if (!connection) {
    connection = await mongoose.connect(url);
    console.log("Connected");
  }
};
