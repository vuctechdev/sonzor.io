import mongoose from "mongoose";
import { connectDB } from "./db";
import { initialValues } from "../../components/blog-form/BlogForm";

const Schema = mongoose.Schema;

const blogSchema = new Schema(
  {
    src: {
      type: String,
      required: true,
    },
    srcMin: {
      type: String,
      required: true,
    },
    en: {
      title: {
        type: String,
        required: true,
      },
      subtitle: {
        type: String,
        required: true,
      },
      content: {
        type: String,
        required: true,
      },
    },
    sr: {
      title: {
        type: String,
        required: true,
      },
      subtitle: {
        type: String,
        required: true,
      },
      content: {
        type: String,
        required: true,
      },
    },
  },
  { timestamps: true }
);

const Blog = mongoose.models.Blog || mongoose.model("Blog", blogSchema);

export interface DynamicContent {
  title: string;
  subtitle: string;
  content: string;
}

export interface BlogType {
  [key: string]: string | DynamicContent;
  _id: string;
  src: string;
  srcMin: string;
  createdAt: string;
  en: DynamicContent;
  sr: DynamicContent;
}

export interface LocalizedBlogType {
  [key: string]: string;
  _id: string;
  src: string;
  srcMin: string;
  createdAt: string;
  title: string;
  subtitle: string;
  content: string;
}

export const getBlogs = async (): Promise<BlogType[]> => {
  await connectDB();
  const blogs = await Blog.find();
  const data = JSON.parse(JSON.stringify(blogs)) as BlogType[];
  return data;
};

export const getLocalizedBlogs = async (
  locale: string
): Promise<LocalizedBlogType[]> => {
  await connectDB();
  const blogs = (await Blog.find()) as BlogType[];
  const data = JSON.parse(JSON.stringify(blogs)) as BlogType[];
  let handler = [] as LocalizedBlogType[];
  data.map((item) => {
    const localizedContent = item[locale] as DynamicContent;
    let itemHandler = {} as LocalizedBlogType;
    Object.keys(item).forEach((key) => {
      if (typeof item[key] === "string") {
        itemHandler = { ...itemHandler, [key]: item[key] as string };
      }
    });
    handler = [...handler, { ...itemHandler, ...localizedContent }];
  });
  return handler;
};

export const createNewBlog = async (data: typeof initialValues) => {
  const blog = new Blog(data);
  const newBlog = await blog.save();
  return newBlog;
};

export const updateBlog = async (data: BlogType) => {
  const updatedBlog = await Blog.findByIdAndUpdate({ _id: data._id }, data, {
    new: true,
  });
  return updatedBlog;
};

export const deleteBlog = async (_id: string) => {
  await Blog.deleteOne({ _id });
  return _id;
};

export const getBlog = async (_id: string): Promise<BlogType> => {
  const schema = new Blog();
  const blog = await schema.findById(_id);
  return blog;
};

export default Blog;
