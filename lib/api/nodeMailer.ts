import nodemailer from "nodemailer";
import { startNewSession } from "./session";

let transporter = nodemailer.createTransport({
  host: process.env.EMAIL_HOST,
  port: Number(process.env.EMAIL_PORT),
  secure: Number(process.env.EMAIL_PORT) === 465, // true for 465, false for other ports
  auth: {
    user: process.env.EMAIL_USER, // generated ethereal user
    pass: process.env.EMAIL_PASSWORD, // generated ethereal password
  },
  tls: {
    rejectUnauthorized: false,
  },
});

interface Data {
  name: string;
  email: string;
  phone: string;
  message: string;
}

export const sendContactMail = async (data: Data) => {
  const { name, email, phone, message } = data;
  const to = process.env.EMAIL_USER;
  try {
    await transporter.sendMail({
      from: `Senzor.io <${process.env.EMAIL_USER}>`, // sender address
      to,
      subject: `Landing Page Contact - ${name}`, // Subject line
      html: `<h5 style="
      display: inline; 
      margin-right: 15px;
      margin-bottom: 20px;
      ">From:</h5><span>${name}</span>
      <br>
      <h5 style="
      display: inline; 
      margin-right: 15px;
      margin-bottom: 20px;
      ">Email:</h5><span>${email}</span>
      <br>
      <h5 style="
      display: inline; 
      margin-right: 15px;
      margin-bottom: 20px;
      ">Phone:</h5><span>${phone}</span>
      <br>
      <h5 style="
      display: inline; 
      margin-right: 15px;
      ">Message:</h5><span>${message}</span>
      `,
    });
  } catch (err) {
    console.log(err);
  }
};

export const sendSessionMail = async (email: string) => {
  const sessionCode = startNewSession();
  try {
    await transporter.sendMail({
      from: `Senzor.io <${process.env.EMAIL_USER}>`, // sender address
      to: email, // list of receivers
      subject: "Your Senzor.io account verification code", // Subject line
      html: `<p style="
      font-size: 14px;
      color: #2a2a2a;
      margin: 4px 0px;
      font-family:'Segoe UI',Tahoma,Verdana,Arial,sans-serif;
      ">To access Senzor.io's admin mode, please use the code below for account verification. The code will only work for 5 minutes.</p>
      <p style="
      padding-top: 20px;
      font-size: 14px;
      color: #2a2a2a;
      font-family:'Segoe UI',Tahoma,Verdana,Arial,sans-serif;
      margin: 2px 0px;
      ">Account verification code:</p>
      <h3 style="
      font-size: 20px;
      color: #2a2a2a;
      margin: 4px 0px;
      font-family:'Segoe UI',Tahoma,Verdana,Arial,sans-serif;
      ">${sessionCode}</h3>`,
    });
  } catch (err) {
    console.log(err);
  }
};
