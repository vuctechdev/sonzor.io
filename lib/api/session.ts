let sessions: { code: string; exp: number }[] = [];

const sessionMinutes = process.env.SESSION_DURATION ?? 10;
const sessionDuration = +sessionMinutes * 1000 * 60;

const source = "ABCDEFGHKLMNQXZWYPRSTUV123456789";
export const generateSessionCode = (length: number = 12) => {
  let code = "";
  for (let i = 0; i < length; i++) {
    const index = Math.floor(Math.random() * source.length) || 1;
    code += source[index - 1];
  }
  return code;
};

export const startNewSession = (): string => {
  const newCode = generateSessionCode();
  sessions = [
    ...sessions,
    { code: newCode, exp: new Date().getTime() + sessionDuration },
  ];
  return newCode;
};

const cleanSessions = () => {
  sessions = sessions.filter(({ exp }) => exp > new Date().getTime());
};

export const validateSession = (sessionCode: string): boolean => {
  cleanSessions();
  return !!sessions.find((item) => item.code === sessionCode);
};
