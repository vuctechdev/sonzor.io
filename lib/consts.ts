export const apiBaseUrl = "/api";
// meta
export const title = "Senzor.io";
export const descriptions: Record<string, string> = {
  sr: "Inovativno rešenje za veći prinos u plasteniku",
  en: "Innovative solution for higher yield in the greenhouse",
};
export const favicon = "/images/favicon.png";
export const webURL = "https://sonzor-io.vercel.app";
export const ogImage = "/images/ogImage.png";
// 180px × 180px
export const appleTouchIcon = "/images/apple_touch_icon.png";
export const GTM_ID = "";
