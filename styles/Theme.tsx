import React, { FC, ReactElement } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { Satoshi, ClashDisplay } from "./localFonts";

declare module "@mui/material/styles" {
  interface TypographyVariants {
    savingsSub: React.CSSProperties;
    savingsMain: React.CSSProperties;
    body3: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    savingsSub: React.CSSProperties;
    savingsMain: React.CSSProperties;
    body3: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    savingsSub: true;
    savingsMain: true;
    body3: true;
  }
}

export const sm = 720;
export const md = 1024;

const h1 = {
  fontSize: "52px",
  fontWeight: 500,
  [`@media (max-width:${sm}px)`]: {
    fontSize: "36px",
  },
};

const h2 = {
  fontSize: "48px",
  fontWeight: 500,
  lineHeight: 1.25,
  [`@media (max-width:${sm}px)`]: {
    fontSize: "30px",
  },
};

const h3 = {
  fontSize: "32px",
  fontWeight: 500,
  [`@media (max-width:${sm}px)`]: {
    fontSize: "28px",
  },
};

const h4 = {
  fontSize: "28px",
  fontWeight: 500,
  [`@media (max-width:${sm}px)`]: {
    fontSize: "24px",
  },
};

const h5 = {
  fontSize: "20px",
  fontWeight: 500,
  [`@media (max-width:${sm}px)`]: {
    fontSize: "18px",
  },
};

const body1 = {
  fontFamily: Satoshi.style.fontFamily,
  fontVariant: Satoshi.style.fontStyle,
  fontSize: "20px",
  fontWeight: 400,
  lineHeight: 1.4,
  [`@media (max-width:${sm}px)`]: {
    fontSize: "16px",
    lineHeight: 1.75,
  },
};

let theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: sm,
      md: md,
      lg: 1500,
      xl: 2000,
    },
  },
  palette: {
    mode: "light",
    primary: {
      dark: "#9bc5b4",
      main: "#ACDBC8",
      light: "#b4dfce",
    },
    secondary: {
      dark: "#8686C3",
      main: "#E7EAF1",
    },
    common: {
      black: "#101013", 
      white: "#fff",
    },
    background: {
      default: "#fff",
    },
  },

  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          "& h1, & h2, & h3, & h4, & h5, & h6": {
            fontFamily: ClashDisplay.style.fontFamily,
            fontVariant: ClashDisplay.style.fontStyle,
          },
          "& p, & a": {
            fontFamily: Satoshi.style.fontFamily,
            fontVariant: Satoshi.style.fontStyle,
          },
        },
        h1,
        h2,
        h3,
        h4,
        h5,
      },
    },
    MuiButton: {
      defaultProps: { variant: "contained" },
      styleOverrides: {
        root: {
          padding: "16px 32px",
          borderRadius: "100px",
        },
      },
    },
    MuiTypography: {
      defaultProps: {
        textAlign: "left",
      },
    },
  },
  typography: {
    allVariants: {
      fontFamily: ClashDisplay.style.fontFamily,
      fontVariant: ClashDisplay.style.fontStyle,
      textAlign: "center",
      lineHeight: 1.3,
      color: "#101013",
    },
    h1,
    h2,
    h3,
    h4,
    h5,
    body1,
    body2: {
      fontFamily: Satoshi.style.fontFamily,
      fontVariant: Satoshi.style.fontStyle,
      fontSize: "14px",
      fontWeight: 400,
      lineHeight: 1.4,
    },
    body3: {
      fontFamily: Satoshi.style.fontFamily,
      fontVariant: Satoshi.style.fontStyle,
      color: "#fff",
      fontSize: "12px",
      fontWeight: 400,
      lineHeight: 1.15,
      [`@media (max-width:${md}px)`]: {
        fontSize: "6.5px",
      },
    },
    savingsMain: {
      fontFamily: ClashDisplay.style.fontFamily,
      fontVariant: ClashDisplay.style.fontStyle,
      fontSize: "64px",
      fontWeight: 500,
      lineHeight: 1,
      [`@media (max-width:${sm}px)`]: {
        fontSize: "40px",
      },
    },
    savingsSub: {
      fontFamily: ClashDisplay.style.fontFamily,
      fontVariant: ClashDisplay.style.fontStyle,
      color: "#868686",
      fontSize: "24px",
      fontWeight: 500,
      [`@media (max-width:${sm}px)`]: {
        fontSize: "18px",
      },
    },
    button: {
      fontFamily: Satoshi.style.fontFamily,
      fontVariant: Satoshi.style.fontStyle,
      color: "#101013",
      fontSize: "18px",
      lineHeight: 1.56,
      fontWeight: 500,
      textTransform: "none",
    },
  },
});

interface ThemeProvider2Props {
  children: any;
}

const ThemeProvider2: FC<ThemeProvider2Props> = ({
  children,
}): ReactElement => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default ThemeProvider2;
