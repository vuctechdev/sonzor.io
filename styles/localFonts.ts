import localFont from "next/font/local";

const Satoshi = localFont({
  src: [
    {
      path: "../public/localFonts/Satoshi/Fonts/WEB/fonts/Satoshi-Medium.woff2",
      weight: "400",
      style: "normal",
    },
    {
      path: "../public/localFonts/Satoshi/Fonts/WEB/fonts/Satoshi-Bold.woff2",
      weight: "700",
      style: "normal",
    },
  ],
});

const ClashDisplay = localFont({
  src: [
    {
      path: "../public/localFonts/ClashDisplay/Fonts/WEB/fonts/ClashDisplay-Medium.woff2",
      weight: "500",
      style: "normal",
    },
    {
      path: "../public/localFonts/ClashDisplay/Fonts/WEB/fonts/ClashDisplay-Bold.woff2",
      weight: "700",
      style: "normal",
    },
  ],
});

export { Satoshi, ClashDisplay };
